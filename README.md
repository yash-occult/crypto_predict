### Installation ###

#### To run this project on colab ####
* Go to (https://colab.research.google.com/)
* Click on file -> Upload notebook -> browse your files and click open
* Upload the dataset available with the notebook to run without errors

#### To run this notebook on your local machine ####
* Install python 3.6 or up from (https://www.python.org/downloads/) 
* Install anaconda based on your os(https://docs.anaconda.com/anaconda/install/index.html)
* After installing anaconda open anaconda prompt
* Install tensorflow by the following command 'conda install -c conda-forge tensorflow'
* Type jupyter notebook 
* A local server will start
* Navigate to your directory and open the .ipynb file

### Notebook Instruction

* With all the packages installed successfully, Run every cell sequentially!
* The notebook predicts prices of cryptocurrencies based using LSTM cells.
* In the notebook, The predicted price is for open price of cryptocurrency which can be changed to 'Open', 'High', 'Low', 'Close', 'Volume' in 6th cell
* Some hyper paramter that can be used to tune and optimize the model are:- length, split_percentage, Number of units in lstms, dropout rate, optimizer, epochs, batch_size,
* At the end of notebook, Predicted and actual price can be seen plotted.